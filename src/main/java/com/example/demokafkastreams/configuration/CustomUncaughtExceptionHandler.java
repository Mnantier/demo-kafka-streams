package com.example.demokafkastreams.configuration;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;

@Slf4j
@EnableIntegration
public class CustomUncaughtExceptionHandler implements StreamsUncaughtExceptionHandler {

    private final StreamBridge streamBridge;

    public CustomUncaughtExceptionHandler(StreamBridge streamBridge) {
        this.streamBridge = streamBridge;
    }

    @Override
    public StreamThreadExceptionResponse handle(Throwable exception) {
        // Send exception to DLQ
        Message<String> message = MessageBuilder
                .withPayload(exception.getMessage())
                .setHeaderIfAbsent("destination", "TOPIC_DLQ")
                .build();
        streamBridge.send("TOPIC_DLQ", message);

        // Log the error for monitoring
        System.out.println("Uncaught exception catched : "+ exception.getMessage());

        // Offsets need to be incremented
        return StreamThreadExceptionResponse.REPLACE_THREAD;
    }
}
