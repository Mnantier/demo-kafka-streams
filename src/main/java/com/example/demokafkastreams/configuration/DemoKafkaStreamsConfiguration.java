package com.example.demokafkastreams.configuration;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.streams.KafkaStreams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.cloud.stream.function.FunctionConfiguration;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.config.KafkaStreamsCustomizer;
import org.springframework.kafka.config.StreamsBuilderFactoryBeanConfigurer;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.CleanupConfig;

@Configuration
@AutoConfigureAfter(FunctionConfiguration.class)
@Slf4j
public class DemoKafkaStreamsConfiguration {

  @Autowired
  public StreamBridge streamBridge;

  @Profile("test")
  @Bean
  public CleanupConfig cleanupConfig() {
    return new CleanupConfig(true, true);
  }

  @Bean
  public CustomUncaughtExceptionHandler customUncaughtExceptionHandler(){
    return new CustomUncaughtExceptionHandler(streamBridge);
  }

  @Bean
  public StreamsBuilderFactoryBeanConfigurer streamsBuilderFactoryBeanConfigurer () {
    return sfb -> {
      sfb.setKafkaStreamsCustomizer(new KafkaStreamsCustomizer() {
        @Override
        public void customize(KafkaStreams kafkaStreams) {
          kafkaStreams.setUncaughtExceptionHandler(new CustomUncaughtExceptionHandler(streamBridge));
        }
      });
    };
  }

  // Create topics on application start
  @Bean
  public NewTopic topicInputA(){
    return TopicBuilder.name("TOPIC_INPUT_A")
            .partitions(1)
            .replicas(1)
            .compact()
            .build();
  }
  @Bean
  public NewTopic topicInputB(){
    return TopicBuilder.name("TOPIC_INPUT_B")
            .partitions(1)
            .replicas(1)
            .compact()
            .build();
  }
  @Bean
  public NewTopic topicOutput(){
    return TopicBuilder.name("TOPIC_OUTPUT")
            .partitions(1)
            .replicas(1)
            .compact()
            .build();
  }
  @Bean
  public NewTopic topicDlq(){
    return TopicBuilder.name("TOPIC_DLQ")
            .partitions(1)
            .replicas(1)
            .compact()
            .build();
  }
}
