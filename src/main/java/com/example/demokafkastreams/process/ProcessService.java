package com.example.demokafkastreams.process;


import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class ProcessService {

  @Bean
  public Function<
          KTable<String, String>,
          Function<
              KTable<String, String>,
                KStream<String, String>>>
                    process() {
    return inputA ->
        inputB -> {
            return inputA
                .join(
                    inputB,
                    (valueA, valueB) -> mapping(valueA, valueB)
                )
                .toStream();
        };
  }

  private String mapping(String valueA, String valueB){
      // throw exception to test the uncaught exception handler
      throw new IllegalStateException("Invalid value");
  }
}
