package com.example.demokafkastreams;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoKafkaStreamsApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoKafkaStreamsApplication.class, args);
	}

}