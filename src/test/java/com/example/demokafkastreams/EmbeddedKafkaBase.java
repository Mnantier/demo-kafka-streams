package com.example.demokafkastreams;

import com.example.demokafkastreams.Generator.KafkaEventGenerator;
import com.example.demokafkastreams.Helper.KafkaTestHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;

import java.util.Collections;
import java.util.Objects;

@EmbeddedKafka(partitions = 1)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@Slf4j
public class EmbeddedKafkaBase {

  protected static EmbeddedKafkaBroker embeddedKafkaBroker;

  protected static final String CONSUMER_GROUP_OUTPUT = "consumer-group-output";
  protected static final String CONSUMER_GROUP_DLQ = "consumer-group-dlq";

  // Configurable Spring context
  private static ConfigurableApplicationContext context;

  // Kafka producers
  protected static KafkaEventGenerator<String, String> inputAGenerator;
  protected static KafkaEventGenerator<String, String> inputBGenerator;

  // Kafka factory consumer
  private static DefaultKafkaConsumerFactory<String, String> outputConsumerFactory;
  private static DefaultKafkaConsumerFactory<?, ?> dlqConsumerFactory;

  // Kafka consumer
  protected Consumer<String, String> outputConsumer;
  protected Consumer<?, ?> dlqConsumer;

  @BeforeAll
  public static void init(EmbeddedKafkaBroker embeddedKafka) {
    embeddedKafkaBroker = embeddedKafka;

    context =
        new SpringApplicationBuilder(DemoKafkaStreamsApplication.class)
            .profiles("test")
            .properties(
                "spring.cloud.stream.kafka.streams.binder.brokers="
                    + embeddedKafka.getBrokersAsString(),
                "spring.cloud.stream.kafka.streams.binder.configuration.commit.interval.ms=10",
                "spring.cloud.stream.kafka.streams.binder.configuration.cache.max.bytes.buffering=0")
            .run();

    inputAGenerator = KafkaTestHelper.buildKafkaEventGenerator(
            embeddedKafka.getBrokersAsString(),
            "TOPIC_INPUT_A",
            KafkaTestHelper.STRING_SERIALIZER,
            KafkaTestHelper.STRING_SERIALIZER
    );

    inputBGenerator = KafkaTestHelper.buildKafkaEventGenerator(
            embeddedKafka.getBrokersAsString(),
            "TOPIC_INPUT_B",
            KafkaTestHelper.STRING_SERIALIZER,
            KafkaTestHelper.STRING_SERIALIZER
    );

    outputConsumerFactory = KafkaTestHelper.buildKafkaConsumerFactory(
            embeddedKafka.getBrokersAsString(),
            CONSUMER_GROUP_OUTPUT,
            KafkaTestHelper.STRING_DESERIALIZER,
            KafkaTestHelper.STRING_DESERIALIZER
    );

    dlqConsumerFactory = KafkaTestHelper.buildKafkaConsumerFactory(
            embeddedKafka.getBrokersAsString(),
            CONSUMER_GROUP_DLQ,
            Serdes.Bytes().deserializer(),
            Serdes.Bytes().deserializer()
    );
  }

  @AfterAll
  public static void shutdown(EmbeddedKafkaBroker embeddedKafka) {
    System.out.println("After all test");
    context.close();
  }

  @BeforeEach
  public void setUp() {
    System.out.println("Before each test");
    outputConsumer = outputConsumerFactory.createConsumer(CONSUMER_GROUP_OUTPUT);
    outputConsumer.subscribe(Collections.singletonList("TOPIC_OUTPUT"));

    dlqConsumer = dlqConsumerFactory.createConsumer(CONSUMER_GROUP_DLQ);
    dlqConsumer.subscribe(Collections.singletonList("TOPIC_DLQ"));
  }

  @AfterEach
  public void tearDown() {
    System.out.println("After each test");
    if (Objects.nonNull(outputConsumer)) {
      outputConsumer.close();
    }
    if (Objects.nonNull(dlqConsumer)) {
      dlqConsumer.close();
    }
  }
}
