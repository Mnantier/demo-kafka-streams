package com.example.demokafkastreams.Generator;

import io.vavr.Tuple3;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serializer;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.List;
import java.util.Map;

@Slf4j
public class KafkaEventGenerator<K, V> {
  private final KafkaTemplate<K, V> kafkaTemplate;
  private final String topic;

  public KafkaEventGenerator(
      Map<String, Object> producerProperties,
      String destination,
      Serializer<K> keySerializer,
      Serializer<V> valueSerializer) {
    DefaultKafkaProducerFactory<K, V> pf =
        new DefaultKafkaProducerFactory<>(producerProperties, keySerializer, valueSerializer);
    kafkaTemplate = new KafkaTemplate<>(pf, true);
    topic = destination;
  }

  public KafkaEventGenerator(
      Map<String, Object> producerProperties,
      Serializer<K> keySerializer,
      Serializer<V> valueSerializer) {
    DefaultKafkaProducerFactory<K, V> pf =
        new DefaultKafkaProducerFactory<>(producerProperties, keySerializer, valueSerializer);
    kafkaTemplate = new KafkaTemplate<>(pf, true);
    topic = "defaultTopic";
  }

  public void doSendEvents(Map<K, V> records) {
    records.forEach(this::doSendEvent);
  }

  public void doSendEvents(List<Tuple3<String, K, V>> records) {
    records.forEach(r -> doSendEvent(r._1, r._2, r._3));
  }

  public void doSendEvent(K key, V value) {
    kafkaTemplate.send(topic, key, value);
  }

  public void doSendEvent(final String topic, K key, V value) {
    kafkaTemplate.send(topic, key, value);
    try {
      Thread.sleep(30L);
    } catch (InterruptedException e) {
      System.out.println("Unable to sleep quietly");
    }
  }
}
