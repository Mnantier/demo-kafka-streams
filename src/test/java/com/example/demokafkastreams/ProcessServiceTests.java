package com.example.demokafkastreams;

import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.junit.jupiter.api.Test;
import org.springframework.kafka.test.utils.KafkaTestUtils;

import static org.assertj.core.api.Assertions.assertThat;

class ProcessServiceTests extends EmbeddedKafkaBase {

	@Test
	void uncaughtExceptionHandlerShouldCatchExceptionAndSendToDLQ() {
		String key = "key";
		String valueA = "valueA";
		String valueB = "valueB";

		// Input some random events, the process should throw an exception for each of them
		inputAGenerator.doSendEvent(key, valueA);
		inputBGenerator.doSendEvent(key, valueB);

		System.out.println("Waiting for records...");
		ConsumerRecords<?,?> records = KafkaTestUtils.getRecords(dlqConsumer, 4000L, 1);
		assertThat(records).isNotNull();
		assertThat(records.count()).isGreaterThanOrEqualTo(1);

	}

}
