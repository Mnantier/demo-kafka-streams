package com.example.demokafkastreams.Helper;

import com.example.demokafkastreams.Generator.KafkaEventGenerator;
import lombok.experimental.UtilityClass;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.*;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;

import java.util.HashMap;
import java.util.Map;

@UtilityClass
public class KafkaTestHelper {
  // SERDES
  public static final StringSerializer STRING_SERIALIZER = new StringSerializer();
  public static final StringDeserializer STRING_DESERIALIZER = new StringDeserializer();

  public static <K, V> KafkaEventGenerator<K, V> buildKafkaEventGenerator(
      String brokersUrl,
      String topic,
      Serializer<K> keySerializer,
      Serializer<V> valueSerializer) {
    Map<String, Object> producerConfig = new HashMap<>();
    producerConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, brokersUrl);
    return new KafkaEventGenerator<>(producerConfig, topic, keySerializer, valueSerializer);
  }

  public static <K, V> DefaultKafkaConsumerFactory<K, V> buildKafkaConsumerFactory(
      String brokersUrl,
      String consumerGroup,
      Deserializer<K> keyDeserializer,
      Deserializer<V> valueDeserializer) {
    Map<String, Object> props = new HashMap<>();
    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, brokersUrl);
    props.put(ConsumerConfig.GROUP_ID_CONFIG, consumerGroup);
    props.put(ConsumerConfig.CLIENT_ID_CONFIG, "test");
    props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true");
    props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "10");
    props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "60000");
    props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
    return new DefaultKafkaConsumerFactory<>(props, keyDeserializer, valueDeserializer);
  }
}
